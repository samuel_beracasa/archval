package org.opalj.av

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.opalj.av.checking.Specification
import org.scalatest.junit.JUnitRunner

/**
 * Displays the architecture of the testClasses and recommends some rules
 * for maintaining this architecture
 * @author Samuel
 *
 */
@RunWith(classOf[JUnitRunner])
class ArchitectureDisplayMath extends FlatSpec with Matchers{
    
    behavior of "the Architecture Validation Library"
    
    it should ("validate the only_allowed constraint functionality") in {
        val expected =
            new Specification {
        		
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            }
        
//        expected.displaySimpleArchitecture(
//        	Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
//        )
        
        expected.displayComplexArchitecture(
        	Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
    }

}