package org.opalj.av

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.scalatest.junit.JUnitRunner

import org.opalj.av.checking.Specification


/**
 * Systematic tests created to check the behavior of the Specification package.
 * Tests that the implemented architecture of the abstract interpretation
 * framework is consistent with its specifiation/with the intended
 * architecture.
 * @author Samuel
 */
@RunWith(classOf[JUnitRunner])
class ArchitectureConsistencyMathTest extends FlatSpec with Matchers{
    
    behavior of "Architecture Validation Library on the Mathematics Test Classes"
    /*
     * Incoming Only_Allowed Constraint Validation
     */
    it should ("validate the only_allowed to be used_by constraint with no violations") in {
        
        val specification1 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Mathematics is_only_allowed(to_be(used_by),'Example)
            	
            }
        
        val result1 = specification1.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result1 should be(empty)
    }
    
    it should ("validate the only_allowed to be used_by constraint with violations") in {
        
        val specification2 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Rational is_only_allowed(to_be(used_by),'Mathematics)
            	
            }
        
        val result2 = specification2.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result2 should not be(empty)
    }
    
    it should ("validate the only_allowed to be(several dependencies) constraint with no violations") in {
        
        val specification3 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Mathematics is_only_allowed(to_be(
            	        called_by,created_by,declared_by),'Example)
            	
            }
        
        val result3 = specification3.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result3 should be(empty)
    }
    
    it should ("validate the only_allowed to be(several dependencies) constraint with violations") in {
        
        val specification4 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Number is_only_allowed(to_be(
            	        created_by,called_by,declared_by),'Rational)
            	
            }
        
        val result4 = specification4.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result4 should not be(empty)
        
        val dependencyList = specification4.created_by.dependencyList ++ 
        					 specification4.called_by.dependencyList ++
        					 specification4.declared_by.dependencyList
        					 
        for(violation<-result4){
            dependencyList should contain(violation.getDependency)
        }
        
    }
    
    it should ("""validate the only_allowed to be(several dependencies) constraint 
            for several Ensembles with no violations""") in {

        val specification5 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Number is_only_allowed(to_be(
            	        created_by, called_by),'Mathematics,'Rational)
            	
            }
        
        val result5 = specification5.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result5 should be(empty)
    }
    
    it should ("""validate the only_allowed to be(several dependencies) constraint 
            for several Ensembles with violations""") in {
        
        val specification6 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Mathematics is_only_allowed(
            	        to_be(created_by, called_by),'Number,'Rational)
            	
            }
        
        val result6 = specification6.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result6 should not be(empty)
        
        val dependencyList = specification6.created_by.dependencyList ++ 
        					 specification6.called_by.dependencyList 
        					 
        for(violation<-result6){
            dependencyList should contain(violation.getDependency)
        }
    }
    
    /*
     * Incoming Not_Allowed Constraint Validation
     */
    it should ("validate the not_allowed to be used_by constraint with no violations") in {
        
        val specification1 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Rational is_not_allowed(to_be(used_by),'Example)
            	
            }
        
        val result1 = specification1.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result1 should be(empty)
    }
    
    it should ("validate the not_allowed to be used_by constraint with violations") in {
        
        val specification2 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Mathematics is_not_allowed(to_be(used_by),'Example)
            	
            }
        
        val result2 = specification2.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result2 should not be(empty)
    }
    
    it should ("validate the not_allowed to be(several dependencies) constraint with no violations") in {
        
        val specification3 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Number is_not_allowed(to_be(
            	        created_by,called_by,declared_by),'Example)
            	
            }
        
        val result3 = specification3.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result3 should be(empty)
    }
    
    it should ("validate the not_allowed to be(several dependencies) constraint with violations") in {
        
        val specification4 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Mathematics is_not_allowed(to_be(
            	        called_by,created_by,declared_by),'Example)
            	
            }
        
        val result4 = specification4.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result4 should not be(empty)
        
        val dependencyList = specification4.created_by.dependencyList ++ 
        					 specification4.called_by.dependencyList ++
        					 specification4.declared_by.dependencyList
        					 
        for(violation<-result4){
            dependencyList should contain(violation.getDependency)
        }
        
        
    }
    
    it should ("""validate the not_allowed to be(several dependencies) constraint 
            for several Ensembles with no violations""") in {

        val specification5 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Mathematics is_not_allowed(
            	        to_be(created_by, called_by),'Number,'Rational)
            	
            }
        
        val result5 = specification5.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result5 should be(empty)
    }
    
    it should ("""validate the not_allowed to be(several dependencies) constraint 
            for several Ensembles with violations""") in {
        
        val specification6 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Number is_not_allowed(to_be(
            	        created_by, called_by),'Mathematics,'Rational)
            	
            }
        
        val result6 = specification6.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result6 should not be(empty)
        
        val dependencyList = specification6.created_by.dependencyList ++ 
        					 specification6.called_by.dependencyList
        					 
        for(violation<-result6){
            dependencyList should contain(violation.getDependency)
        }
    }
    
    /*
     * Incoming Has Constraint Validation
     */
    it should ("validate the has to be used_by constraint with no violations") in {
        
        val specification1 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Mathematics has(to_be(used_by),'Example)
            	
            }
        
        val result1 = specification1.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result1 should be(empty)
    }
    
    it should ("validate the has to be used_by constraint with violations") in {
        
        val specification2 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Rational has(to_be(used_by),'Example)
            	
            }
        
        val result2 = specification2.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result2 should not be(empty)
        result2.size==1 should be(true)
    }
    
    it should ("validate the has to be(several dependencies) constraint with no violations") in {
        
        val specification3 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Mathematics has(to_be(
            	        called_by,created_by,declared_by),'Example)
            	
            }
        
        val result3 = specification3.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result3 should be(empty)
    }
    
    it should ("validate the has to be(several dependencies) constraint with violations") in {
        
        val specification4 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Number has(to_be(
            	        created_by,called_by,declared_by),'Example)
            	
            }
        
        val result4 = specification4.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result4 should not be(empty)
        val dependencyList = specification4.created_by.dependencyList ++ 
        					 specification4.called_by.dependencyList ++
        					 specification4.declared_by.dependencyList
        					 
        for(violation<-result4){
            dependencyList should contain(violation.getDependency)
        }
    }
    
    it should ("""validate the has to be(several dependencies) constraint 
            for several Ensembles with no violations""") in {

        val specification5 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Number has(to_be(
            	        created_by, called_by),'Mathematics,'Rational)
            	
            }
        
        val result5 = specification5.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result5 should be(empty)
        
    }
    
    it should ("""validate the has to be(several dependencies) constraint 
            for several Ensembles with violations""") in {
        
        val specification6 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Mathematics has(
            	        to_be(created_by, called_by),'Number,'Rational)
            	
            }
        
        val result6 = specification6.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result6 should not be(empty)
        val dependencyList = specification6.created_by.dependencyList ++ 
        					 specification6.called_by.dependencyList 
        					 
        for(violation<-result6){
            dependencyList should contain(violation.getDependency)
        }
    }
    
    /*
     * Outgoing Only_Allowed Constraint Validations
     */
    
    it should ("validate the only_allowed to use constraint with no violations") in {
        
        val specification1 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Example is_only_allowed(to(use),'Mathematics)
            	
            }
        
        val result1 = specification1.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result1 should be(empty)
    }
    
    it should ("validate the only_allowed to use constraint with violations") in {
        
        val specification2 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Mathematics is_only_allowed(to(use),'Rational)
            	
            }
        
        val result2 = specification2.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result2 should not be(empty)
    }
    
    it should ("validate the only_allowed to(several dependencies) constraint with no violations") in {
        
        val specification3 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Example is_only_allowed(to(call,create,declare),'Mathematics)
            	
            }
        
        val result3 = specification3.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result3 should be(empty)
    }
    
    it should ("validate the only_allowed to(several dependencies) constraint with violations") in {
        
        val specification4 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Mathematics is_only_allowed(to(create,call,declare),'Rational)
            	
            }
        
        val result4 = specification4.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result4 should not be(empty)
        val dependencyList = specification4.create.dependencyList ++ 
        					 specification4.call.dependencyList ++
        					 specification4.declare.dependencyList
        					 
        for(violation<-result4){
            dependencyList should contain(violation.getDependency)
        }
    }
    
    it should ("""validate the only_allowed to(several dependencies) constraint 
            for several Ensembles with no violations""") in {

        val specification5 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Mathematics is_only_allowed(to(create, call),'Number,'Rational)
            	
            }
        
        val result5 = specification5.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result5 should be(empty)
    }
    
    it should ("""validate the only_allowed to(several dependencies) constraint 
            for several Ensembles with violations""") in {
        
        val specification6 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Mathematics is_only_allowed(to(create, call),'Number,'Operations)
            	
            }
        
        val result6 = specification6.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result6 should not be(empty)
        val dependencyList = specification6.create.dependencyList ++ 
        					 specification6.call.dependencyList
        					 
        for(violation<-result6){
            dependencyList should contain(violation.getDependency)
        }
    }
    
    /*
     * Outgoing Not_Allowed Constraint Validation
     */
    it should ("validate the not_allowed to use constraint with no violations") in {
        
        val specification1 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Example is_not_allowed(to(use),'Rational)
            	
            }
        
        val result1 = specification1.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result1 should be(empty)
    }
    
    it should ("validate the not_allowed to use constraint with violations") in {
        
        val specification2 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Mathematics is_not_allowed(to(use),'Number)
            	
            }
        
        val result2 = specification2.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result2 should not be(empty)
    }
    
    it should ("validate the not_allowed to(several dependencies) constraint with no violations") in {
        
        val specification3 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Mathematics is_not_allowed(to(create,call,declare),'Operations)
            	
            }
        
        val result3 = specification3.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result3 should be(empty)
    }
    
    it should ("validate the not_allowed to(several dependencies) constraint with violations") in {
        
        val specification4 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Example is_not_allowed(to(call,create,declare),'Mathematics)
            	
            }
        
        val result4 = specification4.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result4 should not be(empty)
        val dependencyList = specification4.create.dependencyList ++ 
        					 specification4.call.dependencyList ++
        					 specification4.declare.dependencyList
        					 
        for(violation<-result4){
            dependencyList should contain(violation.getDependency)
        }
    }
    
    it should ("""validate the not_allowed to(several dependencies) constraint 
            for several Ensembles with no violations""") in {

        val specification5 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Mathematics is_not_allowed(to(create, call),'Example,'Operations)
            	
            }
        
        val result5 = specification5.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result5 should be(empty)
    }
    
    it should ("""validate the not_allowed to(several dependencies) constraint 
            for several Ensembles with violations""") in {
        
        val specification6 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            
            	'Mathematics is_not_allowed(to(create, call),'Number,'Rational)
            	
            }
        
        val result6 = specification6.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result6 should not be(empty)
        val dependencyList = specification6.create.dependencyList ++ 
        					 specification6.call.dependencyList ++
        					 specification6.declare.dependencyList
        					 
        for(violation<-result6){
            dependencyList should contain(violation.getDependency)
        }
    }
    
    /*
     * Outgoing Has Constraint Validation
     */
    it should ("validate the has to use constraint with no violations") in {
        
        val specification1 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Example has(to(use),'Mathematics)
            	
            }
        
        val result1 = specification1.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result1 should be(empty)
    }
    
    it should ("validate the has to use constraint with violations") in {
        
        val specification2 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Mathematics has(to(use),'Example)
            	
            }
        
        val result2 = specification2.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result2 should not be(empty)
        result2.size==1 should be(true)
    }
    
    it should ("validate the has to(several dependencies) constraint with no violations") in {
        
        val specification3 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Example has(to(call,create,declare),'Mathematics)
            	
            }
        
        val result3 = specification3.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result3 should be(empty)
        
    }
    
    it should ("validate the has to(several dependencies) constraint with violations") in {
        
        val specification4 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Mathematics has(to(create,call,declare),'Example)
            	
            }
        
        val result4 = specification4.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result4 should not be(empty)
        val dependencyList = specification4.create.dependencyList ++ 
        					 specification4.call.dependencyList ++
        					 specification4.declare.dependencyList
        					 
        for(violation<-result4){
            dependencyList should contain(violation.getDependency)
        }
    }
    
    it should ("""validate the has to(several dependencies) constraint 
            for several Ensembles with no violations""") in {

        val specification5 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Mathematics has(to(create, call),'Number,'Rational)
            	
            }
        
        val result5 = specification5.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result5 should be(empty)
    }
    
    it should ("""validate the has to(several dependencies) constraint
            for several Ensembles with violations""") in {
        
        val specification6 =
            new Specification {
            	ensemble('Operations) {
                    "org.opalj.av.testClasses.Operations*"
                }
            	
            	ensemble('Number) {
                    "org.opalj.av.testClasses.Number*"
                }
            	
            	ensemble('Rational) {
                    "org.opalj.av.testClasses.Rational*"
                }
            	ensemble('Mathematics) {
                    "org.opalj.av.testClasses.Mathematics*"
                }
            	
            	ensemble('Example) {
                    "org.opalj.av.testClasses.Example*"
                }
            	
            	'Mathematics has(to(create, call),'Number,'Operations)
            	
            }
        
        val result6 = specification6.analyze(
                Specification.SourceDirectory("OPAL/av/target/scala-2.11/test-classes")
        )
        
        result6 should not be(empty)
        val dependencyList = specification6.create.dependencyList ++ 
        					 specification6.call.dependencyList
        					 
        for(violation<-result6){
            dependencyList should contain(violation.getDependency)
        }
    }

}