package org.opalj.av

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.BeforeAndAfterAll
import org.scalatest.Matchers
import org.scalatest.junit.JUnitRunner
import org.opalj.av.checking.Specification



/**
 * Systematic tests created to check the behavior of the Specification package.
 * Tests that the implemented architecture of the abstract interpretation
 * framework is consistent with its specifiation/with the intended
 * architecture.
 * @author Samuel
 */
@RunWith(classOf[JUnitRunner])
class ArchitectureConsistencyAITest extends FlatSpec with Matchers with BeforeAndAfterAll {

    behavior of "Architecture Validation Library on the Abstract Interpretation Classes"

    /*
     * Incoming Only_Allowed Constraint Validation
     */
    it should ("validate the only_allowed to be used_by constraint with no violations") in {
        
        val specification1 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Project is_only_allowed(to_be(used_by),'Debug)
                 
            }
        
        val result1 = specification1.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result1 should be(empty)
    }
    
    it should ("validate the only_allowed to be used_by constraint with violations") in {
        
        val specification2 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Util is_only_allowed(to_be(used_by),'Debug)
                 
            }
        
        val result2 = specification2.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result2 should not be(empty)
    }
    
    it should ("validate the only_allowed to be(several dependencies) constraint with no violations") in {
        
        val specification3 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Project is_only_allowed(to_be(called_by,created_by),'Debug)
                 
            }
        
        val result3 = specification3.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result3 should be(empty)
    }
    
    it should ("validate the only_allowed to be(several dependencies) constraint with violations") in {
        
        val specification4 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Util is_only_allowed(to_be(called_by,created_by),'Debug)
                 
            }
        
        val result4 = specification4.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result4 should not be(empty)
        val dependencyList = specification4.created_by.dependencyList ++ 
        					 specification4.called_by.dependencyList
        					 
        for(violation<-result4){
            dependencyList should contain(violation.getDependency)
        }
    }
    
    it should ("""validate the only_allowed to be(several dependencies) constraint 
            for several Ensembles with no violations""") in {

        val specification5 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Core is_only_allowed(
                        to_be(created_by,called_by,declared_by),
                        'Domain_Tracing,'Project,'Domains,'Debug)
                 
            }
        
        val result5 = specification5.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result5 should be(empty)
    }
    
    it should ("""validate the only_allowed to be(several dependencies) constraint 
            for several Ensembles with violations""") in {
        
        val specification6 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Core is_only_allowed(
                        to_be(created_by,called_by,declared_by),
                        'Domain_Tracing,'Util,'Domains,'Debug)
                 
            }
        
        val result6 = specification6.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result6 should not be(empty)
        val dependencyList = specification6.created_by.dependencyList ++ 
        					 specification6.called_by.dependencyList ++
        					 specification6.declared_by.dependencyList
        					 
        for(violation<-result6){
            dependencyList should contain(violation.getDependency)
        }
    }
    
    /*
     * Incoming Not_Allowed Constraint Validation
     */
    it should ("validate the not_allowed to be used_by constraint with no violations") in {
        
        val specification1 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Debug is_not_allowed(to_be(used_by),'Util)
                 
            }
        
        val result1 = specification1.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result1 should be(empty)
    }
    
    it should ("validate the not_allowed to be used_by constraint with violations") in {
        
        val specification2 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Project is_not_allowed(to_be(used_by),'Debug)
                 
            }
        
        val result2 = specification2.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result2 should not be(empty)
    }
    
    it should ("validate the not_allowed to be(several dependencies) constraint with no violations") in {
        
        val specification3 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Debug is_not_allowed(to_be(created_by,called_by),'Project)
                 
            }
        
        val result3 = specification3.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result3 should be(empty)
    }
    
    it should ("validate the not_allowed to be(several dependencies) constraint with violations") in {
        
        val specification4 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Core is_not_allowed(to_be(created_by,called_by),'Project)
                 
            }
        
        val result4 = specification4.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result4 should not be(empty)
        val dependencyList = specification4.created_by.dependencyList ++ 
        					 specification4.called_by.dependencyList
        					 
        for(violation<-result4){
            dependencyList should contain(violation.getDependency)
        }
    }
    
    it should ("""validate the not_allowed to be(several dependencies) constraint 
            for several Ensembles with no violations""") in {

        val specification5 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Debug is_not_allowed(to_be(created_by,called_by),'Project,'Domains,'Core)
                 
            }
        
        val result5 = specification5.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result5 should be(empty)
    }
    
    it should ("""validate the not_allowed to be(several dependencies) constraint 
            for several Ensembles with violations""") in {
        
        val specification6 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Util is_not_allowed(to_be(created_by,called_by),'Project,'Domains,'Core)
                 
            }
        
        val result6 = specification6.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result6 should not be(empty)
        val dependencyList = specification6.created_by.dependencyList ++ 
        					 specification6.called_by.dependencyList
        					 
        for(violation<-result6){
            dependencyList should contain(violation.getDependency)
        }
    }
    
    /*
     * Incoming Has Constraint Validation
     */
    it should ("validate the has to be used_by constraint with no violations") in {
        
        val specification1 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Util has(to_be(used_by),'Core)
                 
            }
        
        val result1 = specification1.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result1 should be(empty)
    }
    
    it should ("validate the has to be used_by constraint with violations") in {
        
        val specification2 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Debug has(to_be(used_by),'Core)
                 
            }
        
        val result2 = specification2.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result2 should not be(empty)
        result2.size==1 should be(true)
    }
    
    it should ("validate the has to be(several dependencies) constraint with no violations") in {
        
        val specification3 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Util has(to_be(created_by,called_by),'Core)
                 
            }
        
        val result3 = specification3.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result3 should be(empty)
    }
    
    it should ("validate the has to be(several dependencies) constraint with violations") in {
        
        val specification4 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Debug has(to_be(created_by,called_by),'Core)
                 
            }
        
        val result4 = specification4.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result4 should not be(empty)
        val dependencyList = specification4.created_by.dependencyList ++ 
        					 specification4.called_by.dependencyList
        					 
        for(violation<-result4){
            dependencyList should contain(violation.getDependency)
        }
    }
    
    it should ("""validate the has to be(several dependencies) constraint 
            for several Ensembles with no violations""") in {

        val specification5 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Util has(to_be(declared_by,called_by),'Debug,'Domain_Tracing)
                 
            }
        
        val result5 = specification5.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result5 should be(empty)
    }
    
    it should ("""validate the has to be(several dependencies) constraint 
            for several Ensembles with violations""") in {
        
        val specification6 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                'Debug has(to_be(created_by,called_by),'Core,'Domains,'Project)
                 
            }
        
        val result6 = specification6.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result6 should not be(empty)
        val dependencyList = specification6.created_by.dependencyList ++ 
        					 specification6.called_by.dependencyList
        					 
        for(violation<-result6){
            dependencyList should contain(violation.getDependency)
        }
    }
    
    /*
     * Outgoing Only_Allowed Constraint Validations
     */
    
    it should ("validate the only_allowed to use constraint with no violations") in {
        
        val specification1 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Util is_only_allowed (to(use),empty)
                 
            }
        
        val result1 = specification1.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result1 should be(empty)
    }
    
    it should ("validate the only_allowed to use constraint with violations") in {
        
        val specification2 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Domain_Tracing is_only_allowed (to(use),'Domains)
                 
            }
        
        val result2 = specification2.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result2 should not be(empty)
    }
    
    it should ("validate the only_allowed to(several dependencies) constraint with no violations") in {
        
        val specification3 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Core is_only_allowed (to(create,call,return_),'Util)
                 
            }
        
        val result3 = specification3.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result3 should be(empty)
    }
    
    it should ("validate the only_allowed to(several dependencies) constraint with violations") in {
        
        val specification4 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Debug is_only_allowed (to(create,call,have_element_of_type),'Util)
                 
            }
        
        val result4 = specification4.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result4 should not be(empty)
        val dependencyList = specification4.create.dependencyList ++ 
        					 specification4.call.dependencyList ++
        					 specification4.have_element_of_type.dependencyList
        					 
        for(violation<-result4){
            dependencyList should contain(violation.getDependency)
        }
    }
    
    it should ("""validate the only_allowed to(several dependencies) constraint 
            for several Ensembles with no violations""") in {

        val specification5 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Project is_only_allowed (to(create,call,have_element_of_type),'Core,'Domains,'Util)
                 
            }
        
        val result5 = specification5.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result5 should be(empty)
    }
    
    it should ("""validate the only_allowed to(several dependencies) constraint 
            for several Ensembles with violations""") in {
        
        val specification6 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Debug is_only_allowed (to(create,call,have_element_of_type),'Util,'Core)
                 
            }
        
        val result6 = specification6.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result6 should not be(empty)
        val dependencyList = specification6.create.dependencyList ++ 
        					 specification6.call.dependencyList ++
        					 specification6.have_element_of_type.dependencyList
        					 
        for(violation<-result6){
            dependencyList should contain(violation.getDependency)
        }
    }
    
    /*
     * Outgoing Not_Allowed Constraint Validation
     */
    it should ("validate the not_allowed to use constraint with no violations") in {
        
        val specification1 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Project is_not_allowed (to(use),'Domain_Tracing)
                 
            }
        
        val result1 = specification1.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result1 should be(empty)
    }
    
    it should ("validate the not_allowed to use constraint with violations") in {
        
        val specification2 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Project is_not_allowed (to(use),'Core)
                 
            }
        
        val result2 = specification2.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result2 should not be(empty)
    }
    
    it should ("validate the not_allowed to(several dependencies) constraint with no violations") in {
        
        val specification3 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Project is_not_allowed (to(create,call,declare),'Domain_Tracing)
                 
            }
        
        val result3 = specification3.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result3 should be(empty)
    }
    
    it should ("validate the not_allowed to(several dependencies) constraint with violations") in {
        
        val specification4 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Project is_not_allowed (to(create,call,declare),'Core)
                 
            }
        
        val result4 = specification4.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result4 should not be(empty)
        val dependencyList = specification4.create.dependencyList ++ 
        					 specification4.call.dependencyList ++
        					 specification4.declare.dependencyList
        					 
        for(violation<-result4){
            dependencyList should contain(violation.getDependency)
        }
    }
    
    it should ("""validate the not_allowed to(several dependencies) constraint 
            for several Ensembles with no violations""") in {

        val specification5 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Project is_not_allowed (to(create,call,declare),'Domain_Tracing,'Debug)
                 
            }
        
        val result5 = specification5.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result5 should be(empty)
    }
    
    it should ("""validate the not_allowed to(several dependencies) constraint 
            for several Ensembles with violations""") in {
        
        val specification6 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Debug is_not_allowed (to(create,call,declare),'Domain_Tracing,'Core,'Project)
                 
            }
        
        val result6 = specification6.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result6 should not be(empty)
        val dependencyList = specification6.create.dependencyList ++ 
        					 specification6.call.dependencyList ++
        					 specification6.declare.dependencyList
        					 
        for(violation<-result6){
            dependencyList should contain(violation.getDependency)
        }
    }
    
    /*
     * Outgoing Has Constraint Validation
     */
    it should ("validate the has to use constraint with no violations") in {
        
        val specification1 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Project has (to(use),'Util)
                 
            }
        
        val result1 = specification1.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result1 should be(empty)
    }
    
    it should ("validate the has to use constraint with violations") in {
        
        val specification2 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Util has (to(use),'Domains)
                 
            }
        
        val result2 = specification2.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result2 should not be(empty)
        result2.size==1 should be(true)
    }
    
    it should ("validate the has to(several dependencies) constraint with no violations") in {
        
        val specification3 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Debug has (to(create,call),'Project)
                 
            }
        
        val result3 = specification3.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result3 should be(empty)
    }
    
    it should ("validate the has to(several dependencies) constraint with violations") in {
        
        val specification4 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Core has (to(create,call),'Domains,'Project)
                 
            }
        
        val result4 = specification4.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result4 should not be(empty)
        val dependencyList = specification4.create.dependencyList ++ 
        					 specification4.call.dependencyList
        					 
        for(violation<-result4){
            dependencyList should contain(violation.getDependency)
        }
    }
    
    it should ("""validate the has to(several dependencies) constraint 
            for several Ensembles with no violations""") in {

        val specification5 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Debug has (to(create,call),'Domains,'Project)
                 
            }
        
        val result5 = specification5.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result5 should be(empty)
    }
    
    it should ("""validate the has to(several dependencies) constraint
            for several Ensembles with violations""") in {
        
        val specification6 =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                'Core has (to(create,call),'Domains,'Project)
                 
            }
        
        val result6 = specification6.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        result6 should not be(empty)
        val dependencyList = specification6.create.dependencyList ++ 
        					 specification6.call.dependencyList
        					 
        for(violation<-result6){
            dependencyList should contain(violation.getDependency)
        }
        
    }
    
}