package org.opalj.av

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import org.opalj.av.checking.Specification
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ExampleTest extends FlatSpec with Matchers{
    
    behavior of "the Abstract Interpretation Framework's implemented architecture"

    it should "be consistent with the specified architecture" in {
        val expected =
            new Specification {
                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }

                /*
                 * The following rules were obtained using the displayArchitecture 
                 * functionality.
                 */
                
                'Debug is_only_allowed(to(call,declare),'Util, 'Core, 'Project, 'Domains)
				'Debug is_not_allowed(to(use),'Domain_Tracing)
				'Domains is_only_allowed(to(call,declare,have_element_of_type),'Util, 'Core)
				'Domains is_not_allowed(to(use),'Debug, 'Domain_Tracing, 'Project)
				'Domain_Tracing is_not_allowed(to(use),'Debug, 'Project)
				'Project is_only_allowed(to(have_element_of_type),'Util, 'Core, 'Domains)
				'Project is_not_allowed(to(use),'Debug, 'Domain_Tracing)
				'Core has(to(call,create,declare,have_element_of_type,return_,typecast),'Util)
				'Util is_only_allowed(to(use),empty)
              
                 
            }
        
        val result6 = expected.analyze(
                Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        
        if (result6.nonEmpty) {
            println("Violations:\n\t"+result6.mkString("\n\t"))
            fail("The implemented and the specified architecture are not consistent (see the console for details).")
        }
        
    }

}