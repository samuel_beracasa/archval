/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj
package av
package checking

import scala.language.implicitConversions

import java.net.URL

import scala.util.matching.Regex
import scala.collection.{ Map ⇒ AMap, Set ⇒ ASet }
import scala.collection.immutable.SortedSet
import scala.collection.mutable.{ Map ⇒ MutableMap, HashSet }

import br._
import br.reader.Java8Framework.ClassFiles
import br.analyses.{ ClassHierarchy, Project }

import de._

/**
 * A specification of a project's architectural constraints.
 *
 * ===Usage===
 * First define the ensembles, then the rules and at last specify the
 * class files that should be analyzed. The rules will then be automatically
 * evaluated.
 *
 * ===Note===
 * One ensemble is predefined: `Specification.empty` it represents an ensemble that
 * contains no source elements and which can, e.g., be used to specify that no "real"
 * ensemble is allowed to depend on a specific ensemble.
 *
 * @author Michael Eichberg
 * @author Samuel Beracasa Beracasa
 */
class Specification {

    @volatile
    private[this] var theEnsembles: MutableMap[Symbol, (SourceElementsMatcher, 
            ASet[VirtualSourceElement])] =
        scala.collection.mutable.OpenHashMap.empty

    /**
     * The set of defined ensembles. An ensemble is identified by a symbol, a query
     * which matches source elements and the project's source elements that are matched.
     * The latter is available only after [[analyze]] was called.
     */
    def ensembles: AMap[Symbol, (SourceElementsMatcher, ASet[VirtualSourceElement])] =
        theEnsembles

    // calculated after all class files have been loaded
    private[this] var theOutgoingDependencies: MutableMap[VirtualSourceElement, 
        AMap[VirtualSourceElement, DependencyTypesSet]] =
        scala.collection.mutable.OpenHashMap.empty

    /**
     * Mapping between a source element and those source elements it depends on/uses.
     *
     * This mapping is automatically created when analyze is called.
     */
    def outgoingDependencies: AMap[VirtualSourceElement, AMap[VirtualSourceElement, 
        DependencyTypesSet]] = theOutgoingDependencies

    // calculated after all class files have been loaded
    private[this] var theIncomingDependencies: MutableMap[VirtualSourceElement, 
        ASet[(VirtualSourceElement, DependencyType)]] =
        scala.collection.mutable.OpenHashMap.empty

    /**
     * Mapping between a source element and those source elements that depend on it.
     *
     * This mapping is automatically created when analyze is called.
     */
    def incomingDependencies: AMap[VirtualSourceElement, ASet[(VirtualSourceElement, 
            DependencyType)]] = theIncomingDependencies

    // calculated after the extension of all ensembles is determined
    private[this] val matchedSourceElements: HashSet[VirtualSourceElement] = HashSet.empty

    private[this] val allSourceElements: HashSet[VirtualSourceElement] = HashSet.empty

    private[this] var unmatchedSourceElements: ASet[VirtualSourceElement] = _

    /**
     * Adds a new ensemble definition to this architecture specification.
     *
     * @throws SpecificationError If the ensemble is already defined.
     */
    @throws(classOf[SpecificationError])
    def ensemble(ensembleSymbol: Symbol)(sourceElementMatcher: SourceElementsMatcher) {
        if (ensembles.contains(ensembleSymbol))
            throw new SpecificationError("The ensemble is already defined: "+ensembleSymbol)

        theEnsembles += (
            (
                ensembleSymbol,
                (sourceElementMatcher, Set[VirtualSourceElement]())
            )
        )
    }

    /**
     * Represents an ensemble that contains no source elements. This can be used, e.g.,
     * to specify that a (set of) specific source element(s) is not allowed to depend
     * on any other source elements (belonging to the project).
     */
    val empty = {
        ensemble('empty)(NoSourceElementsMatcher)
        'empty
    }

    /**
     * Facilitates the definition of common source element matchers by means of common
     * String patterns.
     */
    @throws(classOf[SpecificationError])
    implicit def StringToSourceElementMatcher(matcher: String): SourceElementsMatcher = {
        if (matcher endsWith ".*")
            new PackageNameBasedMatcher(matcher.substring(0, matcher.length() - 2).replace('.', '/'))
        else if (matcher endsWith ".**")
            new PackageNameBasedMatcher(matcher.substring(0, matcher.length() - 3).replace('.', '/'), true)
        else if (matcher endsWith "*")
            new ClassMatcher(matcher.substring(0, matcher.length() - 1).replace('.', '/'), true)
        else if (matcher.indexOf('*') == -1)
            new ClassMatcher(matcher.replace('.', '/'))
        else
            throw new SpecificationError("unsupported pattern: "+matcher);
    }

    def classes(matcher: Regex): SourceElementsMatcher = {
        new RegexClassMatcher(matcher)
    }

    /**
     * Returns the class files stored at the given location.
     */
    implicit def FileToClassFileProvider(file: java.io.File): Seq[(ClassFile, URL)] =
        ClassFiles(file)


   	/**
   	 * Created to maintain a relation between the dependencies passed and the
   	 * printing of the result of the analysis
   	 * @author Samuel Beracasa
   	 */
   	case class DependencyCharacterization(
       name : String,
       dependencyList : List[DependencyType.Value])
    /*
     * Dependency Types definition
     */
    val use = DependencyCharacterization("use",List(DependencyType.USES))
    val used_by = DependencyCharacterization("used_by",List(DependencyType.USES))
    val create = DependencyCharacterization("create",List(DependencyType.CREATES, 
                DependencyType.CREATES_ARRAY, 
                DependencyType.INSTANCE_MEMBER))
    val created_by = DependencyCharacterization("created_by",List(DependencyType.CREATES, 
                DependencyType.CREATES_ARRAY, 
                DependencyType.INSTANCE_MEMBER))
    val extend = DependencyCharacterization("extend",List(DependencyType.EXTENDS, 
                DependencyType.IMPLEMENTS))
    val extended_by = DependencyCharacterization("extended_by",List(DependencyType.EXTENDS, 
                DependencyType.IMPLEMENTS))
    val typecast = DependencyCharacterization("typecast",List(DependencyType.TYPECAST, 
            	DependencyType.TYPECHECK))
    val typecast_of = DependencyCharacterization("typecast_of",List(DependencyType.TYPECAST, 
            	DependencyType.TYPECHECK))
    val return_ = DependencyCharacterization("return_",List(DependencyType.RETURN_TYPE, 
            	DependencyType.RETURN_TYPE_OF_CALLED_METHOD))
    val returned_by = DependencyCharacterization("returned_by",List(DependencyType.RETURN_TYPE, 
            	DependencyType.RETURN_TYPE_OF_CALLED_METHOD))
    val throw_exception_to = DependencyCharacterization("throw_exception_to",List(
            	DependencyType.THROWN_EXCEPTION))
    val catch_exceptions_of = DependencyCharacterization("catch_exception_of",List(
            	DependencyType.CATCHES))
    val catched_by = DependencyCharacterization("catched_by",List(
            	DependencyType.CATCHES))
    val call = DependencyCharacterization("call",List(DependencyType.CALLS_METHOD))
    val called_by = DependencyCharacterization("called_by",List(DependencyType.CALLS_METHOD))
    val annotate = DependencyCharacterization("annotate",List(
	            DependencyType.ANNOTATED_WITH, 
	            DependencyType.PARAMETER_ANNOTATED_WITH,
	            DependencyType.ANNOTATION_DEFAULT_VALUE_TYPE,
	            DependencyType.ANNOTATION_ELEMENT_TYPE))
    val annotated_by = DependencyCharacterization("annotated_by",List(
	            DependencyType.ANNOTATED_WITH, 
	            DependencyType.PARAMETER_ANNOTATED_WITH,
	            DependencyType.ANNOTATION_DEFAULT_VALUE_TYPE,
	            DependencyType.ANNOTATION_ELEMENT_TYPE))
    val have_element_of_type = DependencyCharacterization("have_element_of_type",List(
	            DependencyType.FIELD_TYPE, 
	            DependencyType.CONSTANT_VALUE,
	            DependencyType.PARAMETER_TYPE, 
	            DependencyType.LOCAL_VARIABLE_TYPE))
    val element_of_type = DependencyCharacterization("element_of_type",List(
	            DependencyType.FIELD_TYPE, 
	            DependencyType.CONSTANT_VALUE,
	            DependencyType.PARAMETER_TYPE, 
	            DependencyType.LOCAL_VARIABLE_TYPE))
    val declare = DependencyCharacterization("declare",List(
	            DependencyType.DECLARING_CLASS_OF_ACCESSED_FIELD, 
	            DependencyType.DECLARING_CLASS_OF_CALLED_METHOD))
    val declared_by = DependencyCharacterization("declared_by",List(
	            DependencyType.DECLARING_CLASS_OF_ACCESSED_FIELD, 
	            DependencyType.DECLARING_CLASS_OF_CALLED_METHOD))
	
	            
        
    var dependencyCheckers: List[DependencyChecker] = Nil

    /**
     * supports the only_allowed incoming constraints from an ensemble to 
     * one or more ensembles. When looking for incoming only_allowed 
     * violations, the function will check if a VirtualSourceElement is 
     * allowed to have the specified dependencies coming form other 
     * VirtualSourceElement. The analysis doesn't consider the other 
     * dependencies that were not specified.
     * Example: when element x is only allowed to be created by element z then 
     * the function will check if no other element creates x. In other words, 
     * in the example same example, if element w calls x is acceptable.
     * @author Samuel Beracasa
     */
    case class LocalIncomingOnlyAllowedConstraint(
        sourceEnsemble: Symbol,
        targetEnsembles: Seq[Symbol],
        dependencies: List[DependencyCharacterization])
            extends DependencyChecker {

        override def sourceEnsembles: Seq[Symbol] = Seq(sourceEnsemble)

        override def violations(): ASet[SpecificationViolation] = {
            val sourceElements = ensembles(sourceEnsemble)._2
            val allowedTargetElements = (sourceElements /: 
                    targetEnsembles)(_ ++ ensembles(_)._2)
            val dependencyList = (List[DependencyType]() /: dependencies)(_ ++ _.dependencyList)
            val useDependency = dependencyList.contains(DependencyType.USES)

            for {
                sourceElement ← sourceElements
                targets = incomingDependencies.get(sourceElement)
                if targets.isDefined
                (targetElement, dependencyType) ← targets.get
                /* if useDependency is true (then it doesn't matter what 
                 * dependencyType is)
                 */
                if useDependency || (dependencyList contains dependencyType)
                if !(allowedTargetElements contains targetElement)
                if !(unmatchedSourceElements contains targetElement)
            } yield {
                SpecificationViolationVirtualSourceElements(
                    this,
                    targetElement,
                    sourceElement,
                    dependencyType,
                    "violation of a local incoming only_allowed constraint ")
            }
        }

        override def toString =
            if(dependencies.contains(use)){
            	sourceEnsemble+" is only_allowed to_be used_by ("+targetEnsembles.mkString(",")+")"
            }else{
                sourceEnsemble+" is only_allowed to_be "+dependencies.map(_.name).mkString(", ")+
                " ("+targetEnsembles.mkString(",")+")"
            }
    }

    /**
     * Supports the not_allowed incoming constraints from an ensemble to 
     * one or more ensembles. When looking for incoming not_allowed 
     * violations, the function will check if a VirtualSourceElement 
     * has the specified dependencies coming form other VirtualSourceElement.
     * Example: when element x is not allowed to be created by element z 
     * then the function will check if z creates x and will return a
     * violations if it does.
     * @author Samuel Beracasa
     */
    case class LocalIncomingNotAllowedConstraint(
        sourceEnsemble: Symbol,
        targetEnsembles: Seq[Symbol],
        dependencies: List[DependencyCharacterization])
            extends DependencyChecker {

        override def sourceEnsembles: Seq[Symbol] = Seq(sourceEnsemble)

        override def violations(): ASet[SpecificationViolation] = {
            val sourceElements = ensembles(sourceEnsemble)._2
            val notAllowedTargetElements = (Set[VirtualSourceElement]() /: 
                    targetEnsembles)(_ ++ ensembles(_)._2)
            val dependencyList = (List[DependencyType]() /: dependencies)(_ ++ _.dependencyList)
            val useDependency = dependencyList.contains(DependencyType.USES)

            for {
                sourceElement ← sourceElements
                targets = incomingDependencies.get(sourceElement)
                if targets.isDefined
                (targetElement, dependencyType) ← targets.get
                /* if useDependency is true (then it doesn't matter what 
                 * dependencyType is)
                 */
                if useDependency || (dependencyList contains dependencyType)
                if (notAllowedTargetElements contains targetElement)
                if !(unmatchedSourceElements contains targetElement)
            } yield {
                SpecificationViolationVirtualSourceElements(
                    this,
                    targetElement,
                    sourceElement,
                    dependencyType,
                    "violation of a local incoming not_allowed constraint ")
            }

        }

        override def toString =
            if(dependencies.contains(use)){
            	sourceEnsemble+" is not_allowed to_be used_by ("+targetEnsembles.mkString(",")+")"
            }else{
                sourceEnsemble+" is not_allowed to_be "+dependencies.map(_.name).mkString(", ")+
                " ("+targetEnsembles.mkString(",")+")"
            }
    }

    /**
     * Supports the has_to_be incoming constraints from an ensemble to 
     * one or more ensembles. When looking for incoming has_to_be violations, 
     * the function will check whether a VirtualSourceElement has 
     * the specified dependencies coming form other VirtualSourceElement.
     * Example: when element x has to be created by element z then the 
     * function will check if z creates x and return a violation if it doesn't.
     * @author Samuel Beracasa
     */
    case class LocalIncomingHasToBeConstraint(
        sourceEnsemble: Symbol,
        targetEnsembles: Seq[Symbol],
        dependencies: List[DependencyCharacterization])
            extends DependencyChecker {

        override def sourceEnsembles: Seq[Symbol] = Seq(sourceEnsemble)

        override def violations(): ASet[SpecificationViolation] = {
            var SpecificViolations = ASet[SpecificationViolation]()
            for(targetEnsemble <- targetEnsembles){
                val sourceElements = ensembles(sourceEnsemble)._2
	            val obligatedTargetElements = ensembles(targetEnsemble)._2
	            val dependencyList = (List[DependencyType]() /: dependencies)(_ ++ 
	                    _.dependencyList)
	            val useDependency = dependencyList.contains(DependencyType.USES)
	            val targetsUsed = for {
	                sourceElement ← sourceElements
	                targets = incomingDependencies.get(sourceElement)
	                if targets.isDefined
	                (targetElement, dependencyType) ← targets.get
	                /* if useDependency is true (then it doesn't matter what 
	                 * dependencyType is)
	                 */
	                if useDependency || (dependencyList contains dependencyType)
	                if (obligatedTargetElements contains targetElement)
	                if !(unmatchedSourceElements contains targetElement)
	            } yield {
	                targetElement
	            }
	            
	            // if the list of targetsUsed is empty then there are no dependencies
	            if (targetsUsed.isEmpty) {
	               for(dependency <- dependencyList){
	                    SpecificViolations=SpecificViolations+
	                    		SpecificationViolationEnsembles(this,
			                        targetEnsemble,
			                        sourceEnsemble,
			                        dependency,
			                        "violation of a local incoming has constraint ")
	               }
	                
	            }
            }
            SpecificViolations
        }
        

        override def toString =
            if(dependencies.contains(use)){
            	sourceEnsemble+" has to_be used_by ("+targetEnsembles.mkString(",")+")"
            }else{
                sourceEnsemble+" has to_be "+dependencies.map(_.name).mkString(", ")+
                " ("+targetEnsembles.mkString(",")+")"
            }
    }

    /**
     * Supports the only_allowed outgoing  constraints from an ensemble to 
     * one or more ensembles. When looking for outgoing only_allowed 
     * violations, the function will check if a VirtualSourceElement is 
     * allowed to have the specified dependencies going to other 
     * VirtualSourceElement. The analysis doesn't consider the other 
     * dependencies that were not specified.
     * Example: when element x is only allowed to create element z then the 
     * function will check if x creates other elements that are not z. In 
     * other words, in the example same example, element x calls w is 
     * acceptable.
     * @author Samuel Beracasa
     */
    case class LocalOutgoingOnlyAllowedConstraint(
        sourceEnsemble: Symbol,
        targetEnsembles: Seq[Symbol],
        dependencies: List[DependencyCharacterization])
            extends DependencyChecker {

        override def sourceEnsembles: Seq[Symbol] = Seq(sourceEnsemble)

        override def violations(): ASet[SpecificationViolation] = {
            val unknownEnsembles = targetEnsembles.filterNot(ensembles.contains(_)).mkString(",")
            if (unknownEnsembles.nonEmpty)
                throw new SpecificationError("Unknown ensemble(s): "+unknownEnsembles);
            val sourceElements = ensembles(sourceEnsemble)._2
            val allowedTargetElements = (sourceElements /: 
                    targetEnsembles)(_ ++ ensembles(_)._2)
            val dependencyList = (List[DependencyType]() /: dependencies)(_ ++ _.dependencyList)
            val useDependency = dependencyList.contains(DependencyType.USES)

            for {
                sourceElement ← sourceElements
                targets = outgoingDependencies.get(sourceElement)
                if targets.isDefined
                (targetElement, dependencyTypes) ← targets.get
                dependencyType ← dependencyTypes
                /* if useDependency is true (then it doesn't matter what 
                 * dependencyType is)
                 */
                if useDependency || (dependencyList contains dependencyType)
                if !(allowedTargetElements contains targetElement)
                if !(unmatchedSourceElements contains targetElement)
            } yield {
                SpecificationViolationVirtualSourceElements(
                    this,
                    sourceElement,
                    targetElement,
                    dependencyType,
                    "violation of a local outgoing only allowed constraint")
            }
        }

        override def toString = {
            if(dependencies.contains(use)){
            	sourceEnsemble+" is only_allowed to use ("+targetEnsembles.mkString(",")+")"
            }else{
                sourceEnsemble+" is only_allowed to "+dependencies.map(_.name).mkString(", ")+
                " ("+targetEnsembles.mkString(",")+")"
            }
        }
    }

    /**
     * Supports the not_allowed outgoing constraints from an ensemble to 
     * one or more ensembles. When looking for incoming not_allowed 
     * violations, the function will check if a VirtualSourceElement has 
     * the specified dependencies going to other VirtualSourceElement.
     * Example: when element x is not allowed to create element z then the
     * function will check if x creates z and will return a violations if 
     * it does.
     * @author Samuel Beracasa
     */
    case class LocalOutgoingNotAllowedConstraint(
        sourceEnsemble: Symbol,
        targetEnsembles: Seq[Symbol],
        dependencies: List[DependencyCharacterization])
            extends DependencyChecker {

        override def sourceEnsembles: Seq[Symbol] = Seq(sourceEnsemble)

        override def violations(): ASet[SpecificationViolation] = {
            val unknownEnsembles = targetEnsembles.filterNot(ensembles.contains(_)).mkString(",")
            if (unknownEnsembles.nonEmpty)
                throw new SpecificationError("Unknown ensemble(s): "+unknownEnsembles);
            val sourceElements = ensembles(sourceEnsemble)._2
            val notAllowedTargetElements = (Set[VirtualSourceElement]() /: 
                    targetEnsembles)(_ ++ ensembles(_)._2)
            val dependencyList = (List[DependencyType]() /: dependencies)(_ ++ _.dependencyList)
            val useDependency = dependencyList.contains(DependencyType.USES)

            for {
                sourceElement ← sourceElements
                targets = outgoingDependencies.get(sourceElement)
                if targets.isDefined
                (targetElement, dependencyTypes) ← targets.get
                dependencyType ← dependencyTypes
                /* if useDependency is true (then it doesn't matter what 
                 * dependencyType is)
                 */
                if useDependency || (dependencyList contains dependencyType)
                if (notAllowedTargetElements contains targetElement)
                if !(unmatchedSourceElements contains targetElement)
            } yield {
                SpecificationViolationVirtualSourceElements(
                    this,
                    sourceElement,
                    targetElement,
                    dependencyType,
                    "violation of a local outgoing not allowed constraint ")
            }
        }

        override def toString = {
            if(dependencies.contains(use)){
            	sourceEnsemble+" is not_allowed to use ("+targetEnsembles.mkString(",")+")"
            }else{
                sourceEnsemble+" is not_allowed to "+dependencies.map(_.name).mkString(", ")+
                " ("+targetEnsembles.mkString(",")+")"
            }
        }
    }

    /**
     * Supports the has_to outgoing constraints from an ensemble to one 
     * or more ensembles. When looking for outgoing has_to_be violations, 
     * the function will check whether a VirtualSourceElement has the 
     * specified dependencies going to form other VirtualSourceElement.
     * Example: when element x has to create element z then the function 
     * will check if x creates z and return a violation if it doesn't.
     * @author Samuel Beracasa
     */
    case class LocalOutgoingHasToConstraint(
        sourceEnsemble: Symbol,
        targetEnsembles: Seq[Symbol],
        dependencies: List[DependencyCharacterization])
            extends DependencyChecker {

        override def sourceEnsembles: Seq[Symbol] = Seq(sourceEnsemble)

        override def violations(): ASet[SpecificationViolation] = {
            var SpecificViolations = ASet[SpecificationViolation]()
            for(targetEnsemble <- targetEnsembles){
                val sourceElements = ensembles(sourceEnsemble)._2
	            val obligatedTargetElements = ensembles(targetEnsemble)._2
	            val dependencyList = (List[DependencyType]() /: dependencies)(_ ++ 
	                    _.dependencyList)
	            val useDependency = dependencyList.contains(DependencyType.USES)
	            
	            val targetsUsed = for {
	                sourceElement ← sourceElements
	                targets = outgoingDependencies.get(sourceElement)
	                if targets.isDefined
	                (targetElement, dependencyTypes) ← targets.get
	                dependencyType ← dependencyTypes
	                /* if useDependency is true (then it doesn't matter what 
	                 * dependencyType is)
	                 */
	                if useDependency || (dependencyList contains dependencyType)
	                if (obligatedTargetElements contains targetElement)
	                if !(unmatchedSourceElements contains targetElement)
	            } yield {
	                targetElement
	            }
	            
	            // if the list of targetsUsed is empty then the rule was violated
	            if (targetsUsed.isEmpty) {
	               for(dependency <- dependencyList){
	                    SpecificViolations=SpecificViolations+SpecificationViolationEnsembles(
                        this,
                        targetEnsemble,
                        sourceEnsemble,
                        dependency,
                        "violation of a local outgoing has constraint ")
	               }
	                
	            }
            }
            SpecificViolations
        }

        override def toString = {
            if(dependencies.contains(use)){
            	sourceEnsemble+" has to use ("+targetEnsembles.mkString(",")+")"
            }else{
                sourceEnsemble+" has to "+dependencies.map(_.name).mkString(", ")+
                " ("+targetEnsembles.mkString(",")+")"
            }
        }
    }

    /**
     * Developed to further simplify the semantics of the architecture 
     * specification. It receives all dependency as parameters and returns 
     * them as a list to be passed to the case classes that handle the analysis.
     * the name to refers to outgoing constraints. Example: to(use)
     * @author Samuel Beracasa
     */
    case class to(cons: DependencyCharacterization*){
        def toList = cons.toList
    }

    /**
     * Developed to further simplify the semantics of the architecture 
     * specification. It receives all dependency as parameters and returns 
     * them as a list to be passed to the case classes that handle the analysis.
     * the name to_be refers to incoming constraints. Example: to_be(used_by)
     * @author Samuel Beracasa
     */
    case class to_be(cons: DependencyCharacterization*) {
        def toList = cons.toList
    }

    /**
     * Used for the declaration of the constraints between the Ensembles
     * @author Samuel Beracasa
     */
    case class SpecificationFactory(contextEnsembleSymbol: Symbol) {

        def apply(sourceElementsMatcher: SourceElementsMatcher) {
            ensemble(contextEnsembleSymbol)(sourceElementsMatcher)
        }

        /**
         * @param constraints
         * @param targetEnsembles
         * Used to declare the only_allowed constraints of an ensemble. 
         * Since @param constraints is of class to, the method handles the 
         * outgoing constraints. Both parameters are passed down to class 
         * LocalOutgoingOnlyAllowedConstraint, which executes the analysis. 
         * The returned values from the class LocalOutgoingOnlyAllowedConstraint 
         * are then added to the dependencyCheckers collection.
         */
        def is_only_allowed(constraints: to, targetEnsembles: Symbol*) {
            dependencyCheckers = LocalOutgoingOnlyAllowedConstraint(
                    contextEnsembleSymbol, 
                    targetEnsembles.toSeq, 
                    constraints.toList) :: dependencyCheckers
        }

        /**
         * @param constraints
         * @param targetEnsembles
         * Used to declare the only_allowed constraints of an ensemble. 
         * Since @param constraints is of class to_be, the method handles the 
         * incoming constraints. Both parameters are passed down to class 
         * LocalIncomingOnlyAllowedConstraint, which executes the analysis. 
         * The returned values from the class LocalIncomingOnlyAllowedConstraint 
         * are then added to the dependencyCheckers collection.
         */
        def is_only_allowed(constraints: to_be, targetEnsembles: Symbol*) {
            dependencyCheckers = LocalIncomingOnlyAllowedConstraint(
                    contextEnsembleSymbol, 
                    targetEnsembles.toSeq, 
                    constraints.toList) :: dependencyCheckers
        }

        /**
         * @param constraints
         * @param targetEnsembles
         * Used to declare the not_allowed constraints of an ensemble. 
         * Since @param constraints is of class to, the method handles the 
         * outgoing constraints. Both parameters are passed down to class 
         * LocalOutgoingNotAllowedConstraint, which executes the analysis. 
         * The returned values from the class LocalOutgoingNotAllowedConstraint 
         * are then added to the dependencyCheckers collection.
         */
        def is_not_allowed(constraints: to, targetEnsembles: Symbol*) {
            dependencyCheckers = LocalOutgoingNotAllowedConstraint(
                    contextEnsembleSymbol, 
                    targetEnsembles.toSeq, 
                    constraints.toList) :: dependencyCheckers
        }

        /**
         * @param constraints
         * @param targetEnsembles
         * Used to declare the not_allowed constraints of an ensemble. 
         * Since @param constraints is of class to_be, the method handles the 
         * incoming constraints. Both parameters are passed down to class 
         * LocalIncomingNotAllowedConstraint, which executes the analysis. 
         * The returned values from the class LocalIncomingNotAllowedConstraint 
         * are then added to the dependencyCheckers collection.
         */
        def is_not_allowed(constraints: to_be, targetEnsembles: Symbol*) {
            dependencyCheckers = LocalIncomingNotAllowedConstraint(
                    contextEnsembleSymbol, 
                    targetEnsembles.toSeq, 
                    constraints.toList) :: dependencyCheckers
        }

        /**
         * @param constraints
         * @param targetEnsembles
         * Used to declare the has constraints of an ensemble. 
         * Since @param constraints is of class to, the method handles 
         * the outgoing constraints. Both parameters are passed down to class 
         * LocalOutgoingHasToConstraint, which executes the analysis. The 
         * returned values from the class LocalOutgoingHasToConstraint are then 
         * added to the dependencyCheckers collection.
         */
        def has(constraints: to, targetEnsembles: Symbol*) {
            dependencyCheckers = LocalOutgoingHasToConstraint(
                    contextEnsembleSymbol, 
                    targetEnsembles.toSeq, 
                    constraints.toList) :: dependencyCheckers
        }

        /**
         * @param constraints
         * @param targetEnsembles
         * Used to declare the has constraints of an ensemble. Since 
         * @param constraints is of class to, the method handles the outgoing
         * constraints. Both parameters are passed down to class 
         * LocalIncomingHasToBeConstraint, which executes the analysis. The 
         * returned values from the class LocalIncomingHasToBeConstraint are then 
         * added to the dependencyCheckers collection.
         */
        def has(constraints: to_be, targetEnsembles: Symbol*) {
            dependencyCheckers = LocalIncomingHasToBeConstraint(
                    contextEnsembleSymbol, 
                    targetEnsembles.toSeq, 
                    constraints.toList) :: dependencyCheckers
        }
    }

    protected implicit def EnsembleSymbolToSpecificationElementFactory(
        ensembleSymbol: Symbol): SpecificationFactory =
        SpecificationFactory(ensembleSymbol)

    protected implicit def EnsembleToSourceElementMatcher(
        ensembleSymbol: Symbol): SourceElementsMatcher = {
        if (!ensembles.contains(ensembleSymbol))
            throw new SpecificationError("The ensemble: "+ensembleSymbol+" is not yet defined.")

        ensembles(ensembleSymbol)._1
    }

    /**
     * Returns a textual representation of an ensemble.
     */
    def ensembleToString(ensembleSymbol: Symbol): String = {
        var (sourceElementsMatcher, extension) = ensembles(ensembleSymbol)
        ensembleSymbol+"{"+
            sourceElementsMatcher+"  "+
            {
                if (extension.isEmpty)
                    "/* NO ELEMENTS */ "
                else {
                    (("\n\t//"+extension.head.toString+"\n") /: extension.tail)((s, vse) ⇒ s+"\t//"+vse.toJava+"\n")
                }
            }+"}"
    }

    /**
     * Can be called after the evaluation of the extents of the ensembles to print
     * out the current configuration.
     */
    def ensembleExtentsToString: String = {
        var s = ""
        for ((ensemble, (_, elements)) ← theEnsembles) {
            s += ensemble+"\n"
            for (element ← elements) {
                s += "\t\t\t"+element.toJava+"\n"
            }
        }
        s
    }

    def analyze(classFiles: Traversable[(ClassFile, URL)]): Set[SpecificationViolation] = {

        import util.PerformanceEvaluation.{ ns2sec, time, run }

        // Create and update the support data structures
        //
        val project: Project[URL] = run {
            Project(projectClassFilesWithSources = classFiles)
        } { (executionTime, project) ⇒
            Console.println(
                Console.GREEN+
                    "1. Reading "+
                    project.classFilesCount+" class files took "+
                    ns2sec(executionTime).toString+" seconds."+
                    Console.BLACK)
            project
        }

        val dependencyStore = time {
            project.get(DependencyStoreWithoutSelfDependenciesKey)
        } { executionTime ⇒
            Console.println(
                Console.GREEN+
                    "2.1. Preprocessing dependencies took "+
                    ns2sec(executionTime).toString+" seconds."+
                    Console.BLACK)
        }
        println("Dependencies between source elements: "+dependencyStore.dependencies.size)
        println("Dependencies on primitive types: "+dependencyStore.dependenciesOnBaseTypes.size)
        println("Dependencies on array types: "+dependencyStore.dependenciesOnArrayTypes.size)

        time {
            for {
                (source, targets) ← dependencyStore.dependencies
                (target, dTypes) ← targets
            } {
                allSourceElements += source
                allSourceElements += target

                theOutgoingDependencies.update(source, targets)

                for { dType ← dTypes } {
                    theIncomingDependencies.update(
                        target,
                        theIncomingDependencies.getOrElse(target, Set.empty) +
                            ((source, dType))
                    )
                }
            }
        } { executionTime ⇒
            Console.println(
                Console.GREEN+
                    "2.2. Postprocessing dependencies took "+
                    ns2sec(executionTime).toString+" seconds."+
                    Console.BLACK)
        }
        println("Number of source elements: "+allSourceElements.size)
        println("Outgoing dependencies: "+theOutgoingDependencies.size)
        println("Incoming dependencies: "+theIncomingDependencies.size)

        // Calculate the extension of the ensembles
        //
        time {
            val instantiatedEnsembles =
                theEnsembles.par map { ensemble ⇒
                    val (ensembleSymbol, (sourceElementMatcher, _)) = ensemble
                    // if a sourceElementMatcher is reused!
                    sourceElementMatcher.synchronized {
                        val extension = sourceElementMatcher.extension(project)
                        if (extension.isEmpty && sourceElementMatcher != NoSourceElementsMatcher)
                            Console.println(Console.RED+"   "+ensembleSymbol+" ("+extension.size+")"+Console.BLACK)
                        else
                            Console.println("   "+ensembleSymbol+" ("+extension.size+")")

                        Specification.this.synchronized {
                            matchedSourceElements ++= extension
                        }
                        (ensembleSymbol, (sourceElementMatcher, extension))
                    }
                }
            theEnsembles = instantiatedEnsembles.seq

            unmatchedSourceElements = allSourceElements -- matchedSourceElements

            Console.println("   => Matched source elements: "+matchedSourceElements.size)
            Console.println("   => Other source elements: "+unmatchedSourceElements.size)
        } { executionTime ⇒
            Console.println(
                Console.GREEN+
                    "3. Determing the extension of the ensembles finished in "+
                    ns2sec(executionTime).toString+" seconds."+
                    Console.BLACK)
        }

        // Check all rules
        //
        time {
            val result =
                for (dependencyChecker ← dependencyCheckers.par) yield {
                    Console.println("   Checking: "+dependencyChecker)
                    for (violation ← dependencyChecker.violations) yield {
                        //println(violation)
                        violation
                    }
                }
            Set.empty ++ (result.filter(_.nonEmpty).flatten)
        } { executionTime ⇒
            Console.println(
                Console.GREEN+
                    "4. Checking the specified dependency constraints finished in "+
                    ns2sec(executionTime).toString+
                    " seconds."+
                    Console.BLACK)
        }
    }
    
    val complex = true
    
    class outgoingDependency(
            val fromEnsemble:Symbol,
            var dependencyList:List[DependencyCharacterization],
            val toEnsemble:Symbol=null){
        override def toString()={
            var dependencies = for{dependency <- dependencyList}yield{dependency.name}
            fromEnsemble+" "+
            dependencies.mkString(",")+" "+
            (if(toEnsemble==null){empty}else{toEnsemble})
        }
    }
          
    
    /**
     * @param classFiles
     * Supports the simple printing of a architecture
     */
    def displaySimpleArchitecture(classFiles: Traversable[(ClassFile, URL)]){
        printArchitecture(classFiles,!complex)
    }
    
    /**
     * @param classFiles
     * Supports the complex printing of a architecture 
     */
    def displayComplexArchitecture(classFiles: Traversable[(ClassFile, URL)]){
        printArchitecture(classFiles,complex)
    }
    
    /**
     * @param classFiles
     * @param complexOrSimple
     * Loads the classes and prints the architecture of the program. If the
     * architecture is simple, then it only displays the use relationship 
     * between ensembles. On the other hand, if the architecture is complex 
     * then it displays the specific dependencies between ensembles.  
     */
    private def printArchitecture(
            classFiles: Traversable[(ClassFile, URL)],
            complexOrSimple:Boolean){
        
        import util.PerformanceEvaluation.{ ns2sec, time, run }

        // Create and update the support data structures
        //
        val project: Project[URL] = run {
            Project(projectClassFilesWithSources = classFiles)
        } { (executionTime, project) ⇒
            Console.println(
                Console.GREEN+
                    "1. Reading "+
                    project.classFilesCount+" class files took "+
                    ns2sec(executionTime).toString+" seconds."+
                    Console.BLACK)
            project
        }

        val dependencyStore = time {
            project.get(DependencyStoreWithoutSelfDependenciesKey)
        } { executionTime ⇒
            Console.println(
                Console.GREEN+
                    "2.1. Preprocessing dependencies took "+
                    ns2sec(executionTime).toString+" seconds."+
                    Console.BLACK)
        }
        println("Dependencies between source elements: "+dependencyStore.dependencies.size)
        println("Dependencies on primitive types: "+dependencyStore.dependenciesOnBaseTypes.size)
        println("Dependencies on array types: "+dependencyStore.dependenciesOnArrayTypes.size)

        time {
            for {
                (source, targets) ← dependencyStore.dependencies
                (target, dTypes) ← targets
            } {
                allSourceElements += source
                allSourceElements += target

                theOutgoingDependencies.update(source, targets)

                for { dType ← dTypes } {
                    theIncomingDependencies.update(
                        target,
                        theIncomingDependencies.getOrElse(target, Set.empty) +
                            ((source, dType))
                    )
                }
            }
        } { executionTime ⇒
            Console.println(
                Console.GREEN+
                    "2.2. Postprocessing dependencies took "+
                    ns2sec(executionTime).toString+" seconds."+
                    Console.BLACK)
        }
        println("Number of source elements: "+allSourceElements.size)
        println("Outgoing dependencies: "+theOutgoingDependencies.size)
        println("Incoming dependencies: "+theIncomingDependencies.size)

        // Calculate the extension of the ensembles
        //
        time {
            val instantiatedEnsembles =
                theEnsembles.par map { ensemble ⇒
                    val (ensembleSymbol, (sourceElementMatcher, _)) = ensemble
                    // if a sourceElementMatcher is reused!
                    sourceElementMatcher.synchronized {
                        val extension = sourceElementMatcher.extension(project)
                        if (extension.isEmpty && sourceElementMatcher != NoSourceElementsMatcher)
                            Console.println(Console.RED+"   "+ensembleSymbol+" ("+extension.size+")"+Console.BLACK)
                        else
                            Console.println("   "+ensembleSymbol+" ("+extension.size+")")

                        Specification.this.synchronized {
                            matchedSourceElements ++= extension
                        }
                        (ensembleSymbol, (sourceElementMatcher, extension))
                    }
                }
            theEnsembles = instantiatedEnsembles.seq

            unmatchedSourceElements = allSourceElements -- matchedSourceElements

            Console.println("   => Matched source elements: "+matchedSourceElements.size)
            Console.println("   => Other source elements: "+unmatchedSourceElements.size)
        } { executionTime ⇒
            Console.println(
                Console.GREEN+
                    "3. Determing the extension of the ensembles finished in "+
                    ns2sec(executionTime).toString+" seconds."+
                    Console.BLACK)
        }
        
        var dependencyList = List[outgoingDependency]()

        // Print Architecture
        time {
            if(complexOrSimple!=complex){
                simpleArchitecture
            }else{
                complexArchitecture
            }
        } { executionTime ⇒
            Console.println(
                Console.GREEN+
                    "4. Printing the architecture finished in "+
                    ns2sec(executionTime).toString+
                    " seconds."+
                    Console.BLACK)
        }
        
        //Recommendation of Rules
        time {
            if(complexOrSimple!=complex){
                recommendSimpleArchitecture
            }else{
                recommendComplexArchitecture
            }
        } { executionTime ⇒
            Console.println(
                Console.GREEN+
                    "5. Recommending architecture finished in "+
                    ns2sec(executionTime).toString+
                    " seconds."+
                    Console.BLACK)
        }
        
        def printDependencies(){
            for(dependency<- dependencyList){
                println(dependency.toString)
            }
        }
        
        /**
         * Gets all the ensembles and checks the general use dependency 
         * between all of them. In case a ensembles has no use dependency, 
         * the system returns a dependency to empty 
         */
        def simpleArchitecture(){
            dependencyList = List[outgoingDependency]()
	        var sourceEnsembles = ensembles-empty
	        var targetEnsembles = ensembles-empty
	        var gotDependencies = true
	        for(source<-sourceEnsembles){
	            gotDependencies = false
	            targetEnsembles = sourceEnsembles-source._1
	            for(target<-targetEnsembles){
	                if(getSimpleArchitecture(source._1,target._1)){
	                	dependencyList+:= new outgoingDependency(source._1,List(use),target._1)
	                	gotDependencies = true
	                } 
	            }
	            if(!gotDependencies){
	                dependencyList+:= new outgoingDependency(source._1,List(use),null)
	            }
	        }
            printDependencies()
	        
	        def getSimpleArchitecture(source:Symbol,target:Symbol):Boolean={
		        val sourceElements = ensembles(source)._2
		        val targetElements = ensembles(target)._2
		        for(sourceElement ← sourceElements){
		            val targets = outgoingDependencies.get(sourceElement)
		            if (targets.isDefined){
		                for((targetElement, _) ← targets.get){
		                    if (targetElements contains targetElement){
		                        return true
		                    }
		                }
		            }
		        }
		        false
		    }
        }
        
        /**
         * From the architecture obtained, it checks runs several checks in 
         * order to recommend a architecture:
         * if a ensemble doesn't have dependencies then it uses empty
         * if a ensemble have only one dependency, then the constraint has to
         * use is applied
         * if a ensemble have more than one dependency, then the function
         * checks the number of dependencies against the total number of
         * ensembles. In case the number of dependencies is less than half of
         * the total number, then use the only_allowed. Else use not allowed.  
         */
        def recommendSimpleArchitecture(){
            val ensembleList = ensembles-empty
            for(ensemble <- ensembleList){
                val specificDependencyList = 
                    dependencyList.filter(a=>(a.fromEnsemble==ensemble._1))  
                val AllowedtoEnsembles= 
                    (for{element<-specificDependencyList}yield{element.toEnsemble}).toSet
                val NotAllowedtoEnsembles = 
                        (for{element <-ensembleList 
                            if !(AllowedtoEnsembles contains element._1)
                            if (element!=ensemble)}
                    	yield{element._1})
                if(AllowedtoEnsembles.size==1){
                    if(AllowedtoEnsembles.head==null){
                        println(ensemble._1+" is_only_allowed(to("+use.name+"),empty)")
                    }else{
                    	println(ensemble._1+" has(to("+use.name+"),"+
                    	        AllowedtoEnsembles.mkString(", ")+")")
                    }
                }else{
                    if(AllowedtoEnsembles.size<NotAllowedtoEnsembles.size){
                        println(ensemble._1+" is_only_allowed(to("+
                            use.name+"),"+AllowedtoEnsembles.mkString(", ")+")")
                    }else{
                    	println(ensemble._1+" is_not_allowed(to("+
                            use.name+"),"+NotAllowedtoEnsembles.mkString(", ")+")")
                    }
                }
                
            }
        }
        
        /**
         * From the architecture obtained, it checks runs several checks in 
         * order to recommend a architecture:
         * If a ensemble doesn't have dependencies then it uses empty
         * If a ensemble depends on only one ensemble, then the constraint 
         * has to (list of dependencies) is applied
         * if a ensemble depends on more than one ensemble, then the function
         * checks if there are shared dependencies between all of them. If
         * there are, then the rule only_allowed is applied. Else use not allowed.
         */
        def recommendComplexArchitecture(){
            val ensembleList = ensembles-empty
            for(ensemble <- ensembleList){
                val ensemblesDependenciesList = 
                    dependencyList.filter(a=>(a.fromEnsemble==ensemble._1))  
                val AllowedtoEnsembles= 
                    (for{element<-ensemblesDependenciesList}yield{element.toEnsemble}).toSet
                val NotAllowedtoEnsembles = 
                        (for{element <-ensembleList 
                            if !(AllowedtoEnsembles contains element._1)
                            if (element!=ensemble)}
                    	yield{element._1})
                if(AllowedtoEnsembles.size==1){
                    if(AllowedtoEnsembles.head==null){
                        println(ensemble._1+" is_only_allowed(to("+use.name+"),empty)")
                    }else{
                        val specificDependencyList=
                            for{element<-ensemblesDependenciesList(0).dependencyList}
                        	yield{element.name } 
                    	println(ensemble._1+" has(to("+
                    	        specificDependencyList.mkString(",")+"),"+
                    	        AllowedtoEnsembles.mkString(", ")+")")
                    }
                }else{
                    var allUsedDependenciesList= 
                        for{element<-ensemblesDependenciesList
                            if element.dependencyList.size>1
                            dependency<-element.dependencyList}
                    	yield{dependency}
                    var sharedDependencies=Set[String]()
                    for(usedDependency<-allUsedDependenciesList){
                        var shared=true
                        for(element<-ensemblesDependenciesList){
                            if(!(element.dependencyList contains usedDependency)){
                                shared=false
                            }
                        }
                        if(shared){
                            sharedDependencies+=usedDependency.name 
                        }
                    }
                    if(sharedDependencies.size>0){
                        val toEnsembles = for{element<-ensemblesDependenciesList
                            				}yield{element.toEnsemble}
                    	println(ensemble._1+" is_only_allowed(to("+
                        sharedDependencies.mkString(",")
                        +"),"+toEnsembles.mkString(", ")+")")
                    }
                	println(ensemble._1+" is_not_allowed(to("+
                        use.name+"),"+NotAllowedtoEnsembles.mkString(", ")+")")
                }
                
            }
        }
        
        /**
         * Gets all the ensembles and checks all dependencies between them. 
         * In case a ensembles has no dependency, the system returns 
         * a use dependency to empty 
         */
        def complexArchitecture(){
            dependencyList = List[outgoingDependency]()
	        var sourceEnsembles = ensembles-empty
	        var targetEnsembles = ensembles-empty
	        var gotDependencies = true
	        for(source<-sourceEnsembles){
	            gotDependencies = false
	            targetEnsembles = sourceEnsembles-source._1
	            for(target<-targetEnsembles){
	                val dependencies = getComplexArchitecture(source._1,target._1)
	                if(dependencies.size>0){
	                    dependencyList+:= 
	                        new outgoingDependency(
	                                source._1,
	                                dependencies.toList.sortBy(e=>e.name),
	                                target._1)
	                    gotDependencies = true
	                }
	            }
	            if(!gotDependencies){
	                dependencyList+:= new outgoingDependency(source._1,List(use),null)
	            }
	        }
            printDependencies()
	        
	        def getComplexArchitecture(source:Symbol,target:Symbol):Set[DependencyCharacterization]={
		        val sourceElements = ensembles(source)._2
		        val targetElements = ensembles(target)._2
		        var dependencies = Set[DependencyCharacterization]()
		        for(sourceElement ← sourceElements){
		            val targets = outgoingDependencies.get(sourceElement)
		            if (targets.isDefined){
		                for((targetElement, dependencyTypes) ← targets.get){
		                    if (targetElements contains targetElement){
		                        dependencies++=
		                            getOutgoingDependencies(dependencyTypes)
		                    }
		                }
		            }
		        }
		        dependencies
		    }
	        
	        def getOutgoingDependencies(
	                dependencies:DependencyTypesSet):
	                Set[DependencyCharacterization]={
	            var usedDependencies = Set[DependencyCharacterization]()
	            for(dependency<-dependencies){
	               if(create.dependencyList contains dependency){
	                     usedDependencies += create
	               }else
	               if(extend.dependencyList contains dependency){
	                     usedDependencies += extend
	               }else
	               if(typecast.dependencyList contains dependency){
	                     usedDependencies += typecast
	               }else
	               if(return_.dependencyList contains dependency){
	                     usedDependencies += return_
	               }else
	               if(throw_exception_to.dependencyList contains dependency){
	                     usedDependencies += throw_exception_to
	               }else
	               if(catch_exceptions_of.dependencyList contains dependency){
	                     usedDependencies += catch_exceptions_of
	               }else
	               if(call.dependencyList contains dependency){
	                     usedDependencies += call
	               }else
	               if(annotate.dependencyList contains dependency){
	                     usedDependencies += annotate
	               }else
	               if(have_element_of_type.dependencyList contains dependency){
	                     usedDependencies += have_element_of_type
	               }else
	               if(declare.dependencyList contains dependency){
	                     usedDependencies += declare
	               }
	            }
	            return usedDependencies
	        }
        }
    }
}


object Specification {
    def SourceDirectory(directoryName: String): Seq[(ClassFile, URL)] = {
        val file = new java.io.File(directoryName)
        if (!file.exists)
            throw new SpecificationError("The specified directory does not exist: "+directoryName+".")
        if (!file.canRead)
            throw new SpecificationError("Cannot read the specified directory: "+directoryName+".")
        if (!file.isDirectory)
            throw new SpecificationError("The specified directory is not a directory: "+directoryName+".")

        ClassFiles(file)
    }
}

trait DependencyChecker {

    def violations(): ASet[SpecificationViolation]

    def targetEnsembles: Seq[Symbol]

    def sourceEnsembles: Seq[Symbol]
}
