/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2014
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj
package ai

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FlatSpec
import org.scalatest.BeforeAndAfterAll
import org.scalatest.Matchers

import org.opalj.av.checking.Specification



/**
 * Tests that the implemented architecture of the abstract interpretation
 * framework is consistent with its specifiation/with the intended
 * architecture.
 *
 * @author Michael Eichberg
 */
@RunWith(classOf[JUnitRunner])
class ArchitectureConsistencyTest extends FlatSpec with Matchers with BeforeAndAfterAll {

    behavior of "the Abstract Interpretation Framework's implemented architecture"

    it should "be consistent with the specified architecture" in {
        val expected =
            new Specification {

                ensemble('Core) {
                    "org.opalj.ai.*" except
                        classes("""org\.opalj\.ai\..+Test.*""".r)
                }

                ensemble('Domain_Tracing) {
                    "org.opalj.ai.domain.tracing.*" except
                        classes("""org\.opalj\.ai\.domain\.tracing\..+Test.*""".r)
                }

                ensemble('Util) {
                    "org.opalj.ai.util.*"
                }

                ensemble('Domains) {
                    "org.opalj.ai.domain.*" except
                        classes("""org\.opalj\.ai\.domain\..+Test.*""".r)
                }

                ensemble('Project) {
                    "org.opalj.ai.project.*" except
                        classes("""org\.opalj\.ai\.project\..+Test.*""".r)
                }

                ensemble('Debug) {
                    "org.opalj.ai.debug.*"
                }
                
                //Util constraints
                'Util is_only_allowed (to(use),empty)
                'Util is_not_allowed (to(create),'Core)
                'Util is_not_allowed (to(call),'Domains)
                'Util is_not_allowed (to(throw_exception_to),'Project)
                'Util is_not_allowed (to(have_element_of_type),'Domain_Tracing)
                'Util has (to_be(used_by),'Core)
                
                //Core constraints
                'Core is_only_allowed (to(create,call,have_element_of_type),'Util)
                'Core is_not_allowed (to(create,call),'Domain_Tracing)
                'Core has (to(create,call),'Util)
                'Core has (to_be(used_by),'Project, 'Domain_Tracing)
                'Core has (to_be(created_by,called_by,element_of_type),'Domains,'Project, 'Domain_Tracing)
                
                //Domain constraints
                'Domains is_only_allowed (to(create,call,have_element_of_type),'Util, 'Core)
                'Domains is_only_allowed (to_be(called_by,created_by),'Project, 'Domain_Tracing)
                'Domains is_not_allowed (to(create,call,have_element_of_type),'Project, 'Domain_Tracing)
                'Domains is_not_allowed (to_be(called_by,created_by),'Util, 'Core)
                
                //Project constraints
                'Project has (to(use),'Util, 'Core, 'Domains)
                'Project is_not_allowed (to(use),'Domain_Tracing)
                'Project is_not_allowed (to_be(used_by),'Util, 'Core, 'Domains,'Domain_Tracing)
                'Project has (to(use),'Util, 'Core, 'Domains)
                
                //Domain_Tracing constraints
                'Domain_Tracing is_only_allowed (to(use),'Util, 'Core, 'Domains)                
                'Domain_Tracing is_only_allowed (to_be(used_by),empty)
                'Domain_Tracing is_not_allowed (to(use),'Project )                
                'Domain_Tracing is_not_allowed (to_be(used_by),'Project)
                'Domain_Tracing has (to(use),'Util, 'Core, 'Domains)
                
                // 'Debug is allowed to use everything  
            }

        val result = expected.analyze(
            Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes")
        )
        if (result.nonEmpty) {
            println("Violations:\n\t"+result.mkString("\n\t"))
            fail("The implemented and the specified architecture are not consistent (see the console for details).")
        }
        //expected.printArchitecture(Specification.SourceDirectory("OPAL/ai/target/scala-2.11/classes"))
    }
}